import Buy_Categories from "../components/BuyCategories";
import Layout from "../components/shared/Layout";
import Merch_list from "../components/MerchList/Merch_list";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {useTranslation} from "next-i18next";

const Merch = () => {
    const {t} = useTranslation('merch')
    return (
            <Layout title={t("title")} description={t("description")}>
                <Buy_Categories/>
                <Merch_list/>
            </Layout>
    );
};

export default Merch;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'buy-categories', 'merch']),
    },
})
