import Layout from "../components/shared/Layout";
import Buy_Categories from "../components/BuyCategories";
import Product_list from "../components/ProductList";

import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {useTranslation} from "next-i18next";

const Buy = () => {
    const {t} = useTranslation('product-list')
    const {t: buy} = useTranslation('buy')
    const list = t("list", { returnObjects: true })

    return (
            <Layout title={buy("title")} description={buy("description")}>
                <Buy_Categories/>
                <Product_list list={list}/>
            </Layout>
    );
};

export default Buy;

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'buy-categories', 'product-list', "buy"]),
    },
})
