import Layout from "../components/shared/Layout";
import MainHome from "../components/MainHome";


import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import {useTranslation} from "next-i18next";

export default function Home() {
    const {t} = useTranslation("index")
    return (
        <Layout title={t("title")} description={t("description")}>
            <MainHome/>
        </Layout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'index']),
    },
})
