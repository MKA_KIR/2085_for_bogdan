module.exports = {
    i18n: {
        defaultLocale: 'ua',
        locales: ['ua', 'en'],
    },
    react: {
        useSuspense: false,
        wait: true
    }
}
