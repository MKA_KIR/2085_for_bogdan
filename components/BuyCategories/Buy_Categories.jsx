import {useRouter} from "next/router";
import buy_styles from "../../styles/pages/buy.module.scss";
import Custom_link from "../shared/CustomLink";

import { useTranslation } from 'next-i18next'

const Buy_Categories = () => {

    const { t } = useTranslation('buy-categories')

    const {pathname} = useRouter()

    return (
            <div className={buy_styles.buy_link_container}>
                <Custom_link href={"/buy"} text={t("beer")} className={pathname === '/buy' ? buy_styles.buy_link_active : buy_styles.buy_link}/>
                <Custom_link href={"/merch"} text={t("merch")} className={pathname === '/merch' ? buy_styles.buy_link_active : buy_styles.buy_link}/>
            </div>
    );
};

export default Buy_Categories;
