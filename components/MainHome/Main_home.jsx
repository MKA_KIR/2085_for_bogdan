import Custom_link from "../shared/CustomLink/Custom_link";

import styles from "./main_grid.module.scss";
import {useTranslation} from "next-i18next";

const MainHome = () => {
    const {t} = useTranslation('main-home')
    return (
        <div className={styles.main_grid}>
            <Custom_link href="/read" text={t("read")} className={styles.main_grid_text_read} />
            <Custom_link href="/buy" text={t("buy")} className={styles.main_grid_text_buy}/>
        </div>
    );
};

export default MainHome;
