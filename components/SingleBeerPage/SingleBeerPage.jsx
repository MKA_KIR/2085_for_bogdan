import PropTypes from 'prop-types';
import styles from "./SingleBeerPage.module.scss";

import Image from "next/image";

const SingleBeerPage = ({name, image, price, code, composition, description}) => {
    return (
        <div className={styles.ProductCard}>
            <div className={styles.img}>
                <Image src={image} alt={name} layout="fill" />
            </div>
            <h4 className={styles.name}>{name}</h4>
            <p className={styles.code}>{code}</p>
            <p className={styles.composition}>{composition}</p>
            <p className={styles.description}>{description}</p>
            <p className={styles.price}>{price} UAH</p>
            <button className={styles.btn}>Купити</button>
        </div>
    );
};

SingleBeerPage.propTypes ={
    name: PropTypes.string.isRequired,
    image: PropTypes.string,
    price: PropTypes.number.isRequired,
    code: PropTypes.string.isRequired,
    composition: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired

}
export default SingleBeerPage;
