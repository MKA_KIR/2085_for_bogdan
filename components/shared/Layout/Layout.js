import Navbar from "../Navbar";

import styles from "./layout.module.scss"
import Head from "next/head";
import PropTypes from "prop-types";

export default function Layout({children, title, description}) {
    return (
        <>
            <Head>
                <title>{title}</title>
                <meta name="description" content={description} />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <Navbar/>
            <div className={styles.content}>
                {children}
            </div>
        </>)
}

Layout.defaultProps = {
    title: "2085 new brand of beer",
    description: "2085 new brand of beer"
}

Layout.propTypes ={
    title: PropTypes.string,
    description: PropTypes.string,
}
