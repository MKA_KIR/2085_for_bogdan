import PropTypes from 'prop-types';
import ProductCard from "../ProductCard/ProductCard";

import styles from "./product_list.module.scss";

const ProductList = ({list}) => {
    const productList = list.map(({description, composition, ...rest}) => rest);
    const productElements = productList.map(({id, ...rest}) => <ProductCard key={id} {...rest} />)
    return (
        <div className={styles.container}>
             {productElements}
         </div>
    );
};

ProductList.propTypes ={
    list: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        image: PropTypes.string,
        price: PropTypes.number.isRequired,
    }))

}

export default ProductList;
